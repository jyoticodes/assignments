package com.jyoti.Assignment_3;

import java.util.*;
import java.util.stream.Collectors;

public class PersonUtility {

    /**
     * Q1. Print the avg age of all the people
     */

    public void printAverage(List<Person> person) {
        double average = person.stream().map(p -> p.getAge())
                .mapToDouble(i -> i)
                .average().getAsDouble();
        System.out.println("Average age:  " + average);
    }

    /**
     * Q2. Create a list of all the people who are either greater than 20 or contain any
     * vowel in their name (uppercase or lowercase)
     */

    public List<Person> getPersonAge(List<Person> person) {
        List<Person> listOfPerson = person.stream()
                .filter(p -> p.getAge() > 20 || vowelInName(p))
                .collect(Collectors.toList());
        return listOfPerson;
    }

    private boolean vowelInName(Person p) {
        List<String> vowels = Arrays.asList(new String[]{"a", "e", "i", "o", "u"});
        for (String vowel : vowels) {
            String name = p.getName();
            if (name.contains(vowel) || name.contains(vowel.toUpperCase())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Q3. Create a list of people, sorted in ascending order on the basis of age,
     * if age is the same then sort in descending order of name
     */

    public void sortPersonByAge(List<Person> persons) {
        List<Person> listOfPersons = persons.stream().sorted((p1, p2) -> {
            if (p1.getAge() != p2.getAge()) {
                return p1.getAge() - p2.getAge();
            }
            return p2.getName().compareTo(p1.getName());
        }).collect(Collectors.toList());

        System.out.println(listOfPersons);
    }

    /**
     * Q4. Create a map from this people list where the key is country name and value is count
     * which means a map will tell how many people live in a particular country
     */

    public Map<String, Integer> getPersonByCountry(List<Person> persons) {
        Map<String, Integer> NoofpersonsPerCountry = new HashMap<>();
        persons.stream().forEach(p -> NoofpersonsPerCountry.put(p.getCountry(),
                                 NoofpersonsPerCountry.getOrDefault(p.getCountry(), 0) + 1));
        return NoofpersonsPerCountry;
    }

    /**
     * Q5. Create a map which stores avg age of people per country
     * (key should be country and value should be average age i.e, double)
     */
    public Map<String, Double> getMapOfPeopleByAvgAge(List<Person> persons) {
        Map<String, List<Person>> personMap = getListOfPersonPerCountry(persons);
        Map<String, Double> countryAgeMap = new HashMap<>();
        personMap.entrySet().stream().forEach(es -> {
            countryAgeMap.put(es.getKey(), getAvAge(personMap.get(es.getKey())));
        });

        return countryAgeMap;

    }

    public Map<String, List<Person>> getListOfPersonPerCountry(List<Person> persons) {
        Map<String, List<Person>> personMap = new HashMap<>();
        persons.stream().forEach(p -> {
            String country = p.getCountry();
            List<Person> personsList = personMap.getOrDefault(country, new ArrayList<>());
            personsList.add(p);
            personMap.put(country, personsList);
        });
        return personMap;
    }

    private double getAvAge(List<Person> persons) {
        double age = 0;
        for (Person person : persons) {
            age += person.getAge();
        }

        return age / persons.size();
    }

    /**
     * Q6. Print the oldest person in every country
     */
    public Map<String, Person> printOldestPerson(List<Person> persons) {
        Map<String, Person> oldestPersonInCountry = new HashMap<>();
        Map<String, List<Person>> personMap = getListOfPersonPerCountry(persons);
        personMap.entrySet().stream().forEach(es -> oldestPersonInCountry.put(es.getKey(),
                getMaxAge(es.getValue())));

        return oldestPersonInCountry;
    }

    private Person getMaxAge(List<Person> persons) {
        return persons.stream()
                .max(Comparator.comparing(Person::getAge))
                .get();
    }

    /**
     * Q7.Print the country with most people
     */

    public void countryWithMostPerson(List<Person> persons) {
        Map<String, Integer> personMap = getPersonByCountry(persons);
        System.out.println(personMap.entrySet().stream().max((p1, p2) -> p1.getValue().compareTo(p2.getValue())));
    }

    /**
     * Q8. Create a list of 20 random integers in the range 0 - 1000 using Java 8 streams.
     */

    public List<Integer> generateRandomIntList() {
        return new Random().ints(0, 1001)
                .limit(20).boxed().collect(Collectors.toList());
    }

    public static void main(String[] args) {
        List<Person> people = Arrays.asList(
                new Person(20, "John", "USA"),
                new Person(35, "Sam", "Italy"),
                new Person(15, "Jamie", "England"),
                new Person(30, "Robert", "Italy"),
                new Person(20, "James", "Ireland"),
                new Person(25, "Peter", "USA"),
                new Person(5, "Jessica", "Norway"),
                new Person(40, "Roger", "Netherlands"),
                new Person(50, "Jim", "USA"),
                new Person(12,"zxcvb","India")
        );

       PersonUtility person = new PersonUtility();
       System.out.println("2nd question: "+person.getPersonAge(people));
       person.sortPersonByAge(people);
       System.out.println(person.getPersonByCountry(people));
       System.out.println("Map of people :key is country,value is average age of people living in these country:"+person.getMapOfPeopleByAvgAge(people));
       System.out.println("oldest person in country: "+person.printOldestPerson(people));
       person.countryWithMostPerson(people);
       System.out.println("Random number b/w 1 to 1000: "+person.generateRandomIntList());
    }

}

