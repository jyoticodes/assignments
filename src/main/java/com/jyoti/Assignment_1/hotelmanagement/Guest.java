package com.jyoti.Assignment_1.hotelmanagement;

import java.util.List;

public class Guest {
    private String name;
    private int phoneNo;
    List<Room> roomsBooked;
    private boolean isChecking;

    public Guest(String name, int phoneNo) {
        this.name = name;
        this.phoneNo = phoneNo;
    }

    public String getName() {
        return name;
    }

    public int getPhoneNo() {
        return phoneNo;
    }

    public List<Room> getRoomsBooked() {
        return roomsBooked;
    }

    public boolean isChecking() {
        return isChecking;
    }
}
