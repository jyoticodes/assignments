package com.jyoti.Assignment_1.hotelmanagement;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class HotelManagementService {
    List<Hotel> hotels;

    public List<Hotel> searchByLocation(Address address, Date checkIn,Date checkOut){
        List<Hotel> resHotels = new ArrayList<>();
        for (Hotel hotel: hotels) {
            if (address.getCity().equals(hotel.getAddress().getCity())) {
                resHotels.add(hotel);
            }
        }
        return resHotels;
    }



    public Hotel addHotel(Hotel hotel){
        hotels.add(hotel);
        return hotel;
    }

    public void deleteHotel(Hotel hotel){
        hotels.remove(hotel);
    }

    public List<Hotel> getHotels() {
        return hotels;
    }
}
