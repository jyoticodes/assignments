package com.jyoti.Assignment_1.hotelmanagement;

import java.util.Date;
import java.util.UUID;

public class Booking implements Payment{
    private Hotel hotel;
    private UUID bookingNo;
    private Date checkIn;
    private Date checkOut;

    public Booking(Hotel hotel, int bookingNo) {
        this.hotel = hotel;
        this.bookingNo = UUID.randomUUID();

    }

    @Override
    public void doPayment() {

    }
}
