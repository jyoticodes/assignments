package com.jyoti.Assignment_1.hotelmanagement;

import java.util.ArrayList;
import java.util.List;

public class Hotel {

    private String name;
    private Address address;
    private Rating ratingOfHotel;
    private List<Room> rooms;

    public Hotel(String name, Address address, Rating ratingOfHotel) {
        this.name = name;
        this.address = address;
        this.ratingOfHotel = ratingOfHotel;
        this.rooms = new ArrayList<>();
    }

    public Room addRoom(Room room){
        rooms.add(room);
        return room;
    }

    public void deleteRoom(Room room){
        rooms.remove(room);
    }

    public String getName() {
        return name;
    }

    public Address getAddress() {
        return address;
    }

    public Rating getRatingOfHotel() {
        return ratingOfHotel;
    }

    public List<Room> getRooms() {
        return rooms;
    }
}
