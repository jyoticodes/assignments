package com.jyoti.Assignment_1.ds;

import java.util.HashMap;

/**
 * Q1- You have to implement a function where you are given the root node, you have to return a hashmap in
 * which the key will be the user object and the value would be no of occurrences of that userInfo in the entire tree
 * Note : UserInfo u1 = UserInfo u2 (only and only if all the attributes of a userInfo u1 are equal to
 * all the attributes of userInfo u2).
 */
public class UserEquality {

    HashMap<UserInfo, Integer> map = new HashMap<>();
    public HashMap<UserInfo, Integer> findUserInfoMap(Node root) {

        if (root != null) {
            if (map.containsKey(root.user)) {
                map.put(root.user, map.get(root.user) + 1);
            } else {
                map.put(root.user, 1);
            }

            findUserInfoMap(root.left);
            findUserInfoMap(root.right);
        }

        return map;
    }

    public void traverse(Node root) {
        if (root == null) {
            return;
        }

        System.out.println("root is " + root.user.name);
        traverse(root.left);
        traverse(root.right);

    }

}
