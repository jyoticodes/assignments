package com.jyoti.Assignment_1.ds;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AssigmentOneUtilityClass {
    public static void main(String[] args) {
        UserInfo user1 = new UserInfo(23,"Mike",3);
        UserInfo user2 =  new UserInfo(25,"Micheal",1);
        UserInfo user3 = new UserInfo(23,"Mike",3);
        UserInfo user4 = new UserInfo(12,"Gabby",4);
        UserInfo user5 = new UserInfo(12,"Gabby",4);
        UserInfo user6 = new UserInfo(12,"Gabby",4);
       // UserInfo user5 = new UserInfo(34,"John",5);

        List<UserInfo> list = new ArrayList<>();
        list.add(user1);
        list.add(user2);
        list.add(user3);
        list.add(user4);
       // list.add(user5);

      //  MinimumSumOfUserHeight sum = new MinimumSumOfUserHeight();
       // System.out.println(sum.findMinimumOperationSum(list));

        Node root = new Node(user1);
        root.left = new Node(user2);
        root.right = new Node(user3);
        root.left.left = new Node(user4);
        root.left.right = new Node(user5);
        root.right.left = new Node(user6);
        root.right.right = new Node(user1);
        UserEquality equa = new UserEquality();

        Map<UserInfo,Integer> result = equa.findUserInfoMap(root);
       /*for(Map.Entry<UserInfo,Integer> set : result.entrySet()){
            System.out.println("User : "+set.getKey().name+ " value : "+ set.getValue());
        }*/

       for (UserInfo user:result.keySet()) {
           System.out.println("Name: "+user.name+ " value: "+result.get(user));
       }


    }
}
