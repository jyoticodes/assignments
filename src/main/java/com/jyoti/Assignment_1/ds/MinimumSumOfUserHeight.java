package com.jyoti.Assignment_1.ds;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;

/*
Q2. You are given an array of Users. You have to do the following operation until you are left with
 1 element in the array. In the end, you have to return the minimum sum of all the operation values
  */

public class MinimumSumOfUserHeight {
    public int findMinimumOperationSum(List<UserInfo> users){
        List<Long> height = new ArrayList<>();
        Queue<UserInfo> minHeap = new PriorityQueue<>((a, b) -> (int) (a.height - b.height));
        for(int i=0;i<users.size();i++){
            minHeap.offer(users.get(i));
        }
         while(!minHeap.isEmpty()){
             long sum =0 ;
             UserInfo u1 = minHeap.poll();

            if(!minHeap.isEmpty()) {
                 UserInfo u2 = minHeap.poll();
                sum = u1.getHeight() + u2.getHeight();
                UserInfo u3 = new UserInfo(23,"abc",sum);
                minHeap.offer(u3);
                height.add(sum);
             }
             }
           long total = 0;
         for(int j=0;j<height.size();j++){
             total = total+height.get(j);
         }
        return (int) total;
    }
}
